![casual 3-2.png](https://bitbucket.org/repo/7kAeLn/images/1261991071-casual%203-2.png)# Cloud Runner #
## by Git Gud ##

Cloud Runner is a single player infinite runner cannon launch game. You start the game by selecting your power and angle, and then try and bounce off clouds and other objects to maintain your height, while avoiding birds and other obstacles which slow your progress and knock you down. Other objects, like clouds, will launch the player higher and help them continue on. The players objective is to stay in the air as long as possible and get the best score they can. As a great man once said, "what goes up must come down."

### Matthew Niederberger ###
* Producer
* Programming Support
* Set up terrain generation
* Set up cameras
* Handled repository merging and conflicts
* Set up actual player launching

### Terrance Muchler ###
* Programmer
* Created a basic design plan for the look and behavior of the game
* Determined what obstacles and objects would be and how they effect the player
* Created scripts for each object to effect the player
* Hooked the angle and power sliders to functions so they would effect the cannon

### Chris Granville ###
* Programmer
* Level Designer
* Created models for all obstacles and objects
* Placed the objects into the level and determined where they should go
* Edited broken code and fixed bugs

### Greg Carrobis ###
* Programmer
* UI
* Created launch button
* Created angle and power sliders
* Created left and right arrows to handle player movement
* Set up input manager
* handled score!![casual 3-1.png](https://bitbucket.org/repo/7kAeLn/images/2034980921-casual%203-1.png)