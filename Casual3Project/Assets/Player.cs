﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    [SerializeField]
    float maxPower;

    [SerializeField]
    float maxAngle;

    float power = 0;
    float angle = 0;

    [SerializeField]
    GameObject gameManagerObject;

    GameManager gm;
    MyInputManager inputManager;

    //Vector3 position;

    public float bestDist
    {
        get;
        set;
    }
    public float dist
    {
        get;
        set;
    }

	[SerializeField]
	float maxSpeed;

	//will act as the time limit before the player lands
	float height = 0;

    //Will be the speed at which the player moves forward
    //and the rate at which they gain points
    public float speed = 0;

	float score = 0;

    Rigidbody rb;

	// Use this for initialization
	void Start ()
    {
        dist = 0;

        bestDist = PlayerPrefs.GetFloat("best");

        rb = this.GetComponent<Rigidbody>();

        gm = gameManagerObject.GetComponent<GameManager>();

        inputManager = gameManagerObject.GetComponent<MyInputManager>();
	}
	
	// Update is called once per frame
	void Update () {
		if (gm.HasLaunched)
        {
            if (this.transform.position.y < 0.6f && this.transform.position.x > .5f)
            {
                rb.freezeRotation = true;
                rb.angularVelocity = new Vector3(0.0f, 0.0f, 0.0f);
                rb.velocity = new Vector3(0.0f, 0.0f, 0.0f);

                dist = transform.position.x;
                gm.Touchdown = true;
                if (bestDist < dist)
                {
                    bestDist = dist;
                }
            }

			if(rb.velocity.magnitude > maxSpeed)
			{
				rb.velocity = rb.velocity.normalized * maxSpeed;
			}
        }
	}

    float calculateHeight()
    {
		height = this.transform.position.y;
        return height;
    }
    float calculateSpeed()
    {

        return speed;
    }

    public void Launch()
    {
        rb.freezeRotation = false;
        float xComp = Mathf.Cos(Mathf.Deg2Rad * inputManager.angle);
        float yComp = Mathf.Sin(Mathf.Deg2Rad * inputManager.angle);

        Debug.Log(inputManager.angle);

        xComp *= inputManager.power;
        yComp *= inputManager.power;

        Debug.Log(xComp + ", " + yComp);

        rb.AddForce(xComp, yComp, 0);

        gm.HasLaunched = true;
    }
    
}
