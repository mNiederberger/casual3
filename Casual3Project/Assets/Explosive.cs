﻿using UnityEngine;
using System.Collections;

public class Explosive : MonoBehaviour
{

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void OnTriggerEnter(Collider collider)
    {
        Player p = collider.gameObject.GetComponent<Player>();
        if (p != null)
        {
            p.GetComponent<Rigidbody>().AddForce(500, 3000, 0);
            Destroy(this.gameObject);
        }
    }
}
