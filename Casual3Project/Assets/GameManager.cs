﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour
{
	[SerializeField]
	private GameObject cannonCanvas;

	[SerializeField]
	private GameObject runnerCanvas;

    [SerializeField]
    private GameObject scoreCanvas;

    [SerializeField]
    private GameObject dist;

    Text distText;

    [SerializeField]
    private GameObject bestDist;

    Text bestDistText;

    [SerializeField]
    private GameObject player;

    Player p;

    public bool HasLaunched
    {
        get;
        set;
    }

    public bool Touchdown
    {
        get;
        set;
    }

    // Use this for initialization
    void Start ()
    {
        HasLaunched = false;
        Touchdown = false;

        p = player.GetComponent<Player>();

        distText = dist.GetComponent<Text>();

        bestDistText = bestDist.GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update ()
    {
		if (HasLaunched) {
			cannonCanvas.SetActive(false);
			runnerCanvas.SetActive(true);
            scoreCanvas.SetActive(false);
        } else {
			runnerCanvas.SetActive(false);
			cannonCanvas.SetActive(true);
            scoreCanvas.SetActive(false);
        }

        if (Touchdown)
        {
            cannonCanvas.SetActive(false);
            runnerCanvas.SetActive(false);
            scoreCanvas.SetActive(true);

            distText.text = p.dist.ToString();

            bestDistText.text = p.bestDist.ToString();
        }
        
    }
}
