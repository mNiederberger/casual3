﻿using UnityEngine;
using System.Collections;

public class Airplane : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider collider)
    {
        Player p = collider.gameObject.GetComponent<Player>();
        if (p != null)
        {
            p.GetComponent<Rigidbody>().AddForce(0, -1000, 0);
            //p.speed -= 20;
            Destroy(this.gameObject);
        }
    }
}
