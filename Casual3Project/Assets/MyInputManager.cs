﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MyInputManager : MonoBehaviour {

    [SerializeField]
    Player player;

    Rigidbody rb;

    [SerializeField]
    GameObject gameManagerObject;

    GameManager gm;
    public float angle = 0.0f;
    public float power = 0.0f;

    private float speed = 90.0f;

    // Use this for initialization
    void Start () {
        gm = gameManagerObject.GetComponent<GameManager>();

        rb = player.GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () 
	{

		
	}

    public void chooseAngle(float angle)
    {
        angle *= 90.0f;
        //angle -= speed * Time.deltaTime;
        if (angle < -90.0) angle = -90.0f;
        if (angle > 90) angle = 90.0f;
        //transform.eulerAngles = new Vector3(0, 0, angle);

        this.angle = angle;

        Debug.Log(angle);
    }

    public void choosePower(float _power)
    {
        power = _power;
    }

    public void shootCannon()
    {
        player.Launch();
    }

    public void moveRight()
    {
		if (player.transform.position.z > -10) {
			Vector3 temp = player.transform.position;
			temp.z -= 10;
			player.transform.position = temp;
		}
    }
    public void moveLeft()
    {
		if (player.transform.position.z < 10) {
			Vector3 temp = player.transform.position;
			temp.z += 10;
			player.transform.position = temp;
		}
    }

    public void nextRun()
    {
        gm.Touchdown = false;
        gm.HasLaunched = false;

        player.transform.position = new Vector3(0.0f, 1.0f, 0.0f);
        rb.freezeRotation = true;
        rb.angularVelocity = new Vector3(0.0f, 0.0f, 0.0f);
        rb.velocity = new Vector3(0.0f, 0.0f, 0.0f);

        PlayerPrefs.SetFloat("best", player.bestDist);

        SceneManager.LoadScene("TestScene");
        
    }
}
