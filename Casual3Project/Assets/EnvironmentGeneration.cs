﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnvironmentGeneration : MonoBehaviour
{

    [SerializeField]
    private List<GameObject> groundPrefabs = new List<GameObject>();
    private const float groundLength = 500;

	public GameObject currentGround;
	public GameObject previousGround;
	public GameObject nextGround;

	private float raycastTimer;


	[SerializeField]
    private GameObject player;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
		raycastTimer += Time.deltaTime;

		if (raycastTimer > 1f)
		{
			raycastTimer = 0f;

			RaycastHit hit;
			if (Physics.Raycast(player.transform.position, Vector3.down, out hit, 100000f, LayerMask.GetMask("Terrain")))
			{
				Debug.Log("Ding", hit.collider.gameObject);

				if (hit.transform.gameObject.GetInstanceID() == nextGround.GetInstanceID())
				{
					if (previousGround != null)
					{
						Destroy(previousGround);
					}

					previousGround = currentGround;
					currentGround = nextGround;

					Vector3 pos = currentGround.transform.position;

					nextGround = InstantiateRandomGround(new Vector3(pos.x + groundLength, pos.y, pos.z));

					//Debug.Log(nextGround.transform.position);

				}
			}

			else
			{
				//Destroy everything and make some ground again

				if (previousGround != null)
				{
					Destroy(previousGround);
				}
				if (currentGround != null)
				{
					Destroy(currentGround);
				}
				if (nextGround != null)
				{
					Destroy(nextGround);
				}
                currentGround = InstantiateRandomGround(new Vector3(player.transform.position.x - (groundLength * 0.5f), 0, player.transform.position.z - (groundLength * 0.5f)));
				Vector3 pos = currentGround.transform.position;
				nextGround = InstantiateRandomGround(new Vector3(pos.x + groundLength, pos.y, pos.z));

			}
		}

	}

    private GameObject InstantiateRandomGround(Vector3 position)
    {
        return (GameObject)Instantiate(groundPrefabs[Random.Range(0, groundPrefabs.Count)], position, Quaternion.identity);
    }
}
