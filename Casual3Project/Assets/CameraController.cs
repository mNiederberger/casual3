﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    [SerializeField]
    GameObject managerObject;
    [SerializeField]
    GameObject player;

    Vector3 startPos;

    Quaternion startRot;

    GameManager gm;

	// Use this for initialization
	void Start ()
    {

        gm = managerObject.GetComponent<GameManager>();

        startPos = transform.position;
        startRot = transform.rotation;
	}

    // Update is called once per frame
    void Update()
    {

        if (gm.HasLaunched && !gm.Touchdown)
        {
            transform.position = Vector3.Lerp(transform.position, player.transform.position + (Vector3.left  * 20f), .1f);

            Vector3 f = player.transform.position - transform.position;
            Vector3 u = Quaternion.AngleAxis(-90f, Vector3.back) * f;

            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(f,u), .05f);
        }
        else if (!gm.HasLaunched)
        {
            transform.position = startPos;
            transform.rotation = startRot;
        }

    }
}
