﻿using UnityEngine;
using System.Collections;

public class Cannon : MonoBehaviour {

    public float angle = 0.0f;
    private float speed = 90.0f;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            angle -= speed * Time.deltaTime;
            if (angle < -90.0) angle = -90.0f;
            transform.eulerAngles = new Vector3(0, 0, angle);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            angle -= speed * Time.deltaTime;
            if (angle < 90.0) angle = 90.0f;
            transform.eulerAngles = new Vector3(0, 0, angle);
        }
    }
}
